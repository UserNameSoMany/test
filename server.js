
'use strict'
const io = require('socket.io')(8080)
const redis = require('redis')
const ioRedis = require('socket.io-redis')
const async = require('async')
const env = process.env
const config = {
  port: env.REDIS_PORT || '6379',
  host: env.REDIS_HOST || 'localhost',
  password: env.REDIS_PASSWORD || ''
}
const redisClient = redis.createClient(config.port, config.host, {
  password: config.password
})
const sub = redis.createClient(config.port, config.host, { return_buffers: true, auth_pass: config.password });
io.adapter(ioRedis({ pubClient: redisClient, subClient: sub }))
const teach = io.of('/teach').on('connection', (socket) => {
  let logoneUser = false
  socket.on('kkb.cli.class.join', (data) => {
    socket.fullname = data.fullname
    socket.avatar = data.avatar
    socket.uid = data.uid
    socket.roomid = data.roomid
    socket.roleid = data.roleid
    socket.join(socket.roomid)
    logoneUser = true
    if (socket.roleid !== 0) {
      async.waterfall([
        (callback) => {
          redisClient.hset(`${socket.roomid}teacher`, socket.id, JSON.stringify(data), (err, res) => {
            if (err) {
              console.log(`redis hset ${JSON.stringify(err)}`)
              return
            }
            socket.broadcast.to(socket.roomid).emit('kkb.srv.teacherjoined', {
              fullname: socket.fullname,
              uid: socket.uid,
              avatar: socket.avatar,
              sig: socket.sig
            })
            callback(null, res)
          })
        },
        (res, callback) => {
          if (!res) {
            return callback(new Error('join fail'))
          }
          redisClient.incr(`${socket.roomid}teacherNum`, (err, res) => {
            if (err) {
              return callback(err)
            }
            callback(null, res)
          })
        }
      ], (err, result) => {
        if (err) {
          console.log(`join fail, ${JSON.string(err)}`)
        }
        redisClient.decr(socket.roomid)
      })
    }
    socket.on('kkb.cli.teacher.msg', (data) => {
      teach.to(data.roomid).emit('kkb.srv.teacher.msg', data)
     // io.to(data.roomid).emit('kkb.srv.teacher.msg', data)
    })
    socket.on('disconnect', () => {
      if (logoneUser) {
        async.waterfall([
          (callback) => {
            redisClient.hdel(`${socket.roomid}teacher`, socket.id, (err, res) => {
              if (err) {
                console.log(`redis hset ${JSON.stringify(err)}`)
                return callback(err)
              }
              if (socket.roleid === 1) {
                teach.to(socket.roomid).emit('kkb.srv.teacherlogout', {
                  fullname: socket.fullname,
                  uid: socket.uid,
                  avatar: socket.avatar,
                });
              }
              return callback(null, res)
            })
            
          },
          (res, callback) => {
            if (!res) {
              return callback('disconnect fial')
            }
            redisClient.decr(`${socket.roomid}teacherNum`, (err, res) => {
              if (err) {
                return callback('decr room fail')
              }
              callback(null, res)
            })
          },
          (res, callback) => {
            if (res <= 0) {
              redisClient.expire(`${socket.roomid}teacher`, 0, (err, res) => {
                if (err) {
                  return callback('expire teacher fail')
                }
                redisClient.expire(`${socket.roomid}teacherNum`, 0, (err, res) => {
                  if (err) {
                    return callback('expire teacherNum fail')
                  }
                  callback(null)
                })
              })
            }
          }
        ], (err, result) => {
          if (err) {
            return console.log('have some err')
          }
          console.log(`${socket.fullname} leave ${socket.roomid}`)
        })
      }
    })
  })
})
io.on('connection', (socket) => {
  let logoneUser = false
  socket.on('kkb.cli.message', (data) => {
    if (data.message.length >= 2) {
      io.to(socket.roomid).emit('kkb.srv.message', {
        msg: data.message,
        name: data.userdata.fullname,
        id: data.userdata.uid,
        image: data.userdata.userimg,
        sig: data.userdata.sig
      })
    }
  })
  socket.on('kkb.cli.question', (data) => {
    if (data.message.length >= 10) {
      io.to(socket.roomid).emit('kkb.srv.question', {
        msg: data.message,
        tag: data.tag
      })
    }
  })
  socket.on('kkb.cli.join', (data) => {
    if (logoneUser) return
    console.log(socket.id)
    socket.username = data.fullname
    socket.userimg = data.userimg
    socket.useruid = data.uid
    socket.roomid = data.roomid
    socket.join(socket.roomid)
    let onlineuser = {
      name: socket.username,
      id: socket.useruid,
      img: socket.userimg,
    }
    async.waterfall([
      (callback) => {
        redisClient.hset(`${socket.roomid}user`, socket.id, JSON.stringify(onlineuser), (err, res) => {
          if (err) {
            return callback(new Error(`${socket.fullname} set fail`))
          }
          callback(null, res)
        })
      },
      (res, callback) => {
        if (!res) {
          return callback(new Error(`${socket.fullname} set fail`))
        }
        redisClient.incr(`${socket.roomid}userNum`, (err, res) => {
          if (err) {
            return callback(new Error('set userNum fail'))
          }
          callback(null, res)
        })
      },
      (res, callback) => {
        if (!res) {
          return callback(new Error(`${socket.fullname} set fail`))
        }
        logoneUser = true
        onlineuser.totaluser = res
        socket.emit('kkb.srv.selfjoined', {
          onlineuser: onlineuser
        })
        onlineuser = null
        socket.broadcast.to(socket.roomid).emit('kkb.srv.joined', {
          username: socket.username,
          useruid: socket.userimg,
          userimg:socket.userimg,
          totaluser: res
        })
      }
    ])
  })

  socket.on('disconnect', () => {
    if (logoneUser) {
      async.waterfall([
        (callback) => {
          redisClient.hdel(`${socket.roomid}user`, socket.id, (err, res) => {
            if (err) {
              return callback(new Error(`user ${socket.fullname} disconnect fail`))
            }
            callback(null, res)
          })
        },
        (res, callback) => {
          if (!res) {
            return callback(new Error(`user ${socket.fullname} disconnect fail`))
          }
          redisClient.decr(`${socket.roomid}userNum`, (err, res) => {
            if (err) {
              return callback(new Error(`userNum ${socket.fullname} decr fail`))
            }
            callback(null, res)
          })
        },
        (res, callback) => {
          if (res <= 0) {
            redisClient.expire(`${socket.roomid}user`, 0, (err, res) => {
              if (err) {
                return callback('expire user fail')
              }
              redisClient.expire(`${socket.roomid}userNum`, 0, (err, res) => {
                if (err) {
                  return callback('expire userNum fail')
                }
                callback(null)
              })
            })
          }
        }
      ], (err, result) => {
        if (err) {
          return console.log(JSON.stringify(err))
        }
      })
    }
  })
})