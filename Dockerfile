# Dockerfile to create a node image
FROM index.tenxcloud.com/tenxcloud/nodejs:5.11.1
MAINTAINER kkb

# Add files to the image
ADD . /opt/nodejs
WORKDIR /opt/nodejs

# Install the dependencies modules
RUN npm install

# Expose the container port
EXPOSE 8080

ENTRYPOINT ["node", "server.js"]

