var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var kkb_roomusers = {};
var kkb_roomteachers = {};
// const ioRedis = require('socket.io-redis')
// io.adapter(ioRedis({ host: 'localhost', port: 6379 }));

//老师讲课区
var teach = io
    .of('/teach')
    .on('connection', function (socket) {
      var logonedUser = false;

      socket.on('kkb.cli.class.join',function(data){
        //老师的一些信息用来传递相应的信息
        socket.fullname = data.fullname;
        socket.avatar = data.avatar;
        socket.uid= data.uid;
        socket.roomid = data.roomid;
        socket.roleid = data.roleid;
        socket.sig=data.sig;

        socket.join(socket.roomid);

        if(socket.roleid===1||socket.roleid<0){
          kkb_roomteachers[socket.roomid]=data; //保存房间对应的老师信息
        }

        logonedUser = true;
        //告诉大家老师进来了

        log("判断roleid:" + socket.roleid);


        if(socket.roleid===1||socket.roleid<0){
          socket.broadcast.to(socket.roomid).emit('kkb.srv.teacherjoined', {
            //teach.to(data.roomid).emit('kkb.srv.teacherjoined', {
            fullname: socket.fullname,
            uid: socket.uid,
            avatar: socket.avatar,
            sig:socket.sig
          });
        }
      });

      //判断如何是老师则可以发送信息到房间
      socket.on('kkb.cli.teacher.msg',function(data){
        //socket.broadcast.to(socket.roomid).emit('kkb.srv.teacher.msg', data);
        teach.to(data.roomid).emit('kkb.srv.teacher.msg', data);
      });

      //只需处理老师离线
      socket.on('disconnect', function () {
        if (logonedUser) {
          if(socket.roleid===1){
            //socket.broadcast.to(socket.roomid).emit('kkb.srv.teacherlogout', {
            teach.to(socket.roomid).emit('kkb.srv.teacherlogout', {
              fullname: socket.fullname,
              uid: socket.uid,
              avatar: socket.avatar,
            });
          }
        }
      });
    });

//学生们讨论区
io.on('connection', function(socket){
  var logonedUser = false;
  console.log(socket.id)
  //发布同一个房间里的消息
  socket.on('kkb.cli.message', function(data){
    if(data.message.length>=2){
      io.to(socket.roomid).emit('kkb.srv.message',{
            msg:data.message,
            name:data.userdata.fullname,
            id:data.userdata.uid,
            image:data.userdata.userimg,
            sig:data.userdata.sig
          }
      );
    }else{
      return false;
    }

  });
  //发布同一个房间里的问题提问
  socket.on('kkb.cli.question', function(data){
    if(data.message.length>=10){
      io.to(socket.roomid).emit('kkb.srv.question',{
            msg:data.message,
            tag:data.tag
          }

      );
    }else{
      return false;
    }

  });
  //用户加入
  socket.on('kkb.cli.join', function (data) {
    //log('讨论区加入的人是：' + data.fullname+',他的id是:'+data.uid);
    if (logonedUser) return;
    socket.username = data.fullname;
    socket.userimg = data.userimg;
    socket.useruid=data.uid;
    socket.roomid = data.roomid;
    socket.join(socket.roomid);

    var userlist = kkb_roomusers[socket.roomid];
    if(userlist == null){
      kkb_roomusers[socket.roomid]=[];
    }

    kkb_roomusers[socket.roomid].push({name:socket.username,id:socket.useruid,img:socket.userimg});

    logonedUser = true;

    socket.emit('kkb.srv.selfjoined', {
      onlineuser: kkb_roomusers[socket.roomid]
    });

    socket.broadcast.to(socket.roomid).emit('kkb.srv.joined', {
      username: socket.username,
      useruid: socket.useruid,
      userimg:socket.userimg,
      totaluser: kkb_roomusers[socket.roomid].length
    });
  });

  socket.on('disconnect', function () {
    if (logonedUser) {
      //离开的人对其进行分析
      var userlist = kkb_roomusers[socket.roomid];
      var removeIndex = userlist.map(function(item){ return item.id; }).indexOf(socket.useruid);
      removeIndex >= 0 && userlist.splice(removeIndex, 1);
      kkb_roomusers[socket.roomid] = userlist;
      log('讨论区离开的人是：' +socket.username +',他的id是:'+socket.useruid);
      socket.broadcast.to(socket.roomid).emit('kkb.srv.logout', {
        totaluser:userlist.length,
        username: socket.username
      });
    }
  });

});

//http.listen(8008, function(){
//  log('服务器启动 *:8080');
//});

http.listen(8080, function(){
  log('服务器启动 *:8080');
});

/*
 * LOG utility
 */
var gbdebug = true;

function log(info){
  if(gbdebug){
    console.log('DEBUG:: ' + info);
  }
}

function error(info){
  if(gbdebug){
    console.log('WARNING:: ' + info);
  }
}

Array.prototype.indexOf = function(val) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == val) return i;
  }
  return -1;
};
Array.prototype.remove = function(val) {
  var index = this.indexOf(val);
  if (index > -1) {
    this.splice(index, 1);
  }
};
